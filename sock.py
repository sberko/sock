import socket, select, argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-p', action='store', default="8080", dest='PORT', help='Port')
    args = parser.parse_args()
    CONNECTION_ADDR = {}
    CONNECTION_LIST = []
    RECV_BUFFER = 4096
    PORT = int(args.PORT) if args.PORT.isdecimal() else 8080
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)

    CONNECTION_LIST.append(server_socket)
    print("Port " + str(PORT))

    while True:
        read_sockets, write_sockets, error_sockets = select.select(CONNECTION_LIST, [], [])

        for sock in read_sockets: #type: socket.socket

            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                CONNECTION_ADDR[addr[0]] = 0
                CONNECTION_LIST.append(sockfd)
            else:
                try:
                    data = sock.recv(RECV_BUFFER) #type: bytes
                    if data:
                        if data.decode().strip() == 'stop':
                                raise Exception()
                        if data.decode().strip() == 'list':
                            bar = bytearray(str(CONNECTION_ADDR.items()), encoding='ascii')
                            sock.send(bar + b'\r\n')
                        else:
                            CONNECTION_ADDR[addr[0]] += int(data.decode())
                            bar = bytearray(str(CONNECTION_ADDR[addr[0]]), encoding='ascii')
                            sock.send(b'=' + bar + b'\r\n')
                except ValueError as err:
                    sock.send(b"Send int please\r\n")
                except Exception as err:
                    print(err)
                    print("Client ({}) disconnect".format(sock.getpeername()))
                    sock.close()
                    CONNECTION_ADDR.pop(addr[0])
                    CONNECTION_LIST.remove(sock)
                    continue
